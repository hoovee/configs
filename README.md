# configs

Configurations for computers, servers, etc.

## Generate quotes

Install

    sudo apt install cowsay lolcat fortune figlet

    sudo strfile softwareengineering

Copy both `softwareengineering` and `softwareengineering.dat` to `/usr/share/games/fortunes/`.
